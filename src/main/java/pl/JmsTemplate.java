package pl;

public interface JmsTemplate {

    public void convertAndSend(String msg);
}
