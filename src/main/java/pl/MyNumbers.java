package pl;

import java.util.List;

public class MyNumbers {
    private final List<Integer> numbers;
    private final JmsTemplate jms;

    public MyNumbers(List<Integer> numbers, JmsTemplate jms) {
        this.numbers = numbers;
        this.jms = jms;
    }

    public int largest() {
        int i, max = Integer.MIN_VALUE;
        jms.convertAndSend(MyNumbers.class.getName() + " was used");
        for (i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) > max) {
                max = numbers.get(i);
            }
        }
        return max;
    }
}
