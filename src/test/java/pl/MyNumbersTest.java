package pl;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MyNumbersTest {

    @Test
    void largest() {
        //given
        final MyNumbers objectUnderTest = new MyNumbers(Arrays.asList(8, 9, 7), null);

        //when
        int result = objectUnderTest.largest();

        //then
        assertEquals(9, result);
    }

    @Test
    void shouldReturnLargestFromLastPosition() {
        //given
        final MyNumbers objectUnderTest = new MyNumbers(Arrays.asList(8, 9, 10), null);

        //when
        int result = objectUnderTest.largest();

        //then
        assertEquals(10, result);
    }

    @Test
    void shouldReturnLargestFromListWithDuplicates() {
        //given
        final MyNumbers objectUnderTest = new MyNumbers(Arrays.asList(10, 8, 9, 10), null);
        //when
        int actual = objectUnderTest.largest();
        //then
        assertEquals(10, actual);
    }

    @Test
    void shouldReturnLargestFromListWithNegativeValues() {
        //given
        final MyNumbers objectUnderTest = new MyNumbers(Arrays.asList(-10, -8, -9), null);
        //when
        int actual = objectUnderTest.largest();
        //then
        assertEquals(-8, actual);
    }

    @Test
    void shouldReturnLargestFromListWithSingleElement() {
        //given
        final MyNumbers objectUnderTest = new MyNumbers(Arrays.asList(9), null);
        //when
        int actual = objectUnderTest.largest();
        //then
        assertEquals(9, actual);
    }

    @DisplayName("Test finding largest")
    @ParameterizedTest(name = "{2} - {1} is the largest value of {0}")
    @MethodSource("inputData")
    void parametrizedTests(List<Integer> input, int expected, String comment) {
        //given
        final MyNumbers objectUnderTest = new MyNumbers(input, null);

        //when
        int actual = objectUnderTest.largest();

        //then
        assertEquals(expected, actual);
    }

    private static Stream<Arguments> inputData() {
        return Stream.of(
                Arguments.of(Arrays.asList(8, 9, 7), 9, "Just largest"),
                Arguments.of(Arrays.asList(8, 9, 10), 10, "Largest last"),
                Arguments.of(Arrays.asList(10, 8, 9, 10), 10, "Duplicates"),
                Arguments.of(Arrays.asList(-10, -8, -9), -8, "Negative numbers"),
                Arguments.of(Arrays.asList(9), 9, "Single element")
        );
    }
}