package pl;

import name.falgout.jeffrey.testing.junit5.MockitoExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import java.util.Collections;

@ExtendWith(MockitoExtension.class)
class MyNumbersTestWithMocking {

    @Mock
    private JmsTemplate jmsMock;

    @Test
    void largestShouldSendEvent() {
        //given
        MyNumbers objectUnderTest = new MyNumbers(Collections.EMPTY_LIST, jmsMock);

        //when
        objectUnderTest.largest();

        //then
        Mockito.verify(jmsMock, Mockito.times(1)).convertAndSend("pl.MyNumbers was used");
    }
}